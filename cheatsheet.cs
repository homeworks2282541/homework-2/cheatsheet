using System.Collections.Generic;

/******************************************
 * ARRAY / LIST
 */

int[] array = new int[] {1, 2, 3, 4, 5};

// LOOP
foreach (int item in array)
{
  Console.Write(item + " ");
}

// GET
int x = array[1];
Console.WriteLine(x);

// SET
array[3] = 10;
Console.WriteLine(array[3]);

// EXISTS KEY/VALUE
Console.WriteLine(array[3] != null);

/******************************************
 * DICTIONARY
 */
Dictionary<string, string> dict = new Dictionary<string, string>();
dict["key3"] = "new value 3";

// ADD/SET
dict["key2"] = "new value";
Console.WriteLine(dict["key2"]);

// LOOP
foreach (var item in dict)
{
  Console.WriteLine(item.Key + " : " + item.Value);
}

// REMOVE
dict.Remove("key2");

// GET
string y = dict["key3"];
Console.WriteLine(y);

// EXISTS KEY/VALUE
Console.WriteLine(dict.ContainsKey("key2"));

/******************************************
 * SORTED LIST
 */
SortedList<string, int> sortedList = new SortedList<string, int>();
sortedList["x"] = 5;

// LOOP
foreach (var item in sortedList)
{
  Console.WriteLine(item.Key + " : " + item.Value);
}

// ADD
sortedList["newKey"] = 40;

// REMOVE
sortedList.Remove("newKey");

// GET
Console.WriteLine(sortedList["x"]);

// SET
sortedList["x"] = 6;

// EXISTS KEY/VALUE
sortedList.ContainsKey("x");

/******************************************
 * HASHSET
 */
HashSet<int> hashSet = new HashSet<int>();

// LOOP
foreach (var item in hashSet)
{
  Console.WriteLine(item);
}

// ADD
hashSet.Add(6);
hashSet.Add(5);
hashSet.Add(2);

// REMOVE
hashSet.Remove(5);

// EXISTS KEY/VALUE
hashSet.Contains(5);

/******************************************
 * SORTED SET
 */
SortedSet<int> sortedSet = new SortedSet<int>();
sortedSet.Add(5);

// LOOP
foreach (var item in sortedSet)
{
  Console.WriteLine(item);
}

// ADD
sortedSet.Add(50);

// REMOVE
sortedSet.Remove(50);

// EXISTS KEY/VALUE
sortedSet.Contains(5);

/******************************************
 * QUEUE
 */
Queue<int> queue = new Queue<int>();
queue.Enqueue(5);
queue.Enqueue(3);
queue.Enqueue(1);
queue.Enqueue(7);

// LOOP
foreach (var item in queue)
{
  Console.WriteLine(item);
}

// ADD
queue.Enqueue(8);

// REMOVE
queue.Dequeue();

// EXISTS KEY/VALUE
queue.Contains(5);

/******************************************
 * STACK
 */
Stack<int> stack = new Stack<int>();
stack.Push(1);
stack.Push(2);
stack.Push(3);

// LOOP
foreach (var item in stack)
{
  Console.WriteLine(item);
}

// ADD / SET
stack.Push(2);

// REMOVE / GET
stack.Pop();

// EXISTS KEY/VALUE
stack.Contains(1);

/******************************************
 * LinkedList
 */
LinkedList<int> ll = new LinkedList<int>();

// LOOP
foreach (var item in ll)
{
    Console.WriteLine(item);
}

// ADD
ll.AddLast(4);
ll.AddLast(7);
ll.AddLast(9);

// REMOVE
ll.Remove(7);

// EXISTS KEY/VALUE
ll.Contains(9);
