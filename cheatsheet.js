/******************************************
 * ARRAY / LIST 
 */
const array = [1, 2, 3, 4, 5];

// LOOP
array.forEach(i => console.log(i));

// ADD
array.push(6);

// REMOVE
let index = array.indexOf(2);
index > -1 && array.splice(index, 1);

// GET
array[1];

// SET
index = array.indexOf(3);
index > -1 && array.splice(index, 1, 5);

// EXISTS KEY/VALUE
array.includes(2);

/******************************************
 * DICTIONARY 
 */
const dict = {
    key: 'val1'
};

// LOOP
Object.values(dict).forEach(i => console.log(i));

// ADD/SET
dict['key2'] = 'new value';

// REMOVE
delete dict.key2;

// GET
dict?.key;

// EXISTS KEY/VALUE
const exists = dict.key2 !== undefined && dict.key2 !== null;

/******************************************
 * SORTED LIST 
 */
const sortedList = [1, 2, 4, 4, 3];
sortedList.sort();

// LOOP
sortedList.forEach(i => console.log(i));

// ADD
sortedList.push(6);
sortedList.sort();

// REMOVE
index = sortedList.indexOf(2);
index > -1 && sortedList.splice(index, 1);

// GET
array[1];

// SET
index = sortedList.indexOf(3);
index > -1 && sortedList.splice(index, 1, 5);
sortedList.sort();

// EXISTS KEY/VALUE
sortedList.includes(2);

/******************************************
 * HASHSET
 */
const hashSet = new Set([1, 5, 3, 6, 7]);

// LOOP
hashSet.forEach(i => console.log(i));

// ADD
hashSet.add(1);

// REMOVE
hashSet.delete(1);

// GET
hashSet.values().next();

// EXISTS KEY/VALUE
hashSet.has(3);

/******************************************
 * SORTED SET 
 */
let sortedSet = new Set([1, 5, 3, 6, 7].sort());

// LOOP
sortedSet.forEach(i => console.log(i));

// ADD
sortedSet = new Set(Array.from(sortedSet.add(1)));

// REMOVE
sortedSet.delete(1);

// GET
sortedSet.values().next();

// EXISTS KEY/VALUE
sortedSet.has(3);

/******************************************
 * QUEUE
 */
const queue = [2, 5, 3, 8, 4];

// LOOP
queue.forEach(i => console.log(i));

// ADD / SET
queue.push(2);

// REMOVE / GET
queue.shift();

// EXISTS KEY/VALUE
queue.includes(3);

/******************************************
 * STACK
 */
const stack = [6, 3, 6, 8, 3, 2];

// LOOP
stack.forEach(i => console.log(i));

// ADD / SET
stack.push(2);

// REMOVE / GET
stack.pop();

// EXISTS KEY/VALUE
stack.includes(3);

/******************************************
 * LINKED LIST 
 */
class Node {
    constructor(data) {
        this.data = data;
        this.next = null;
    }
}

class LinkedList {
    #size = 0;
    #head = null;

    constructor() { }

    #increaseSize() { this.#size++; }
    #decreaseSize() { this.#size--; }

    getSize() { return this.#size; }

    add(data) {
        if (!this.#head) {
            this.#head = new Node(data);
        } else {
            let current = this.#head;

            while (current.next) current = current.next;

            current.next = new Node(data);
        }
        this.#increaseSize();
    }

    get() {
        let current = this.#head;

        while (current.next) current = current.next;

        return current.data;
    }

    removeAt(index) {
        if (index >= this.#size || index < 0) return

        let current = this.#head;
        let prev = this.#head;
        let counter = 0;

        while (counter !== index) {
            prev = current;
            current = current.next;
            counter++;
        }

        prev.next = current.next;
        current = null;
        this.#decreaseSize();

        return current.data;
    }

    loop() {
        let current = this.#head;

        while (current) {
            console.log(current.data);
            current = current.next;
        }
    }

    exists(data) {
        if (this.#size === 0) return false;

        let current = this.#head;
        let found = false;

        while (current) {
            if (current.data === data) {
            found = true;
            break;
            }
            current = current.next;
        }

        return found;
    }
}